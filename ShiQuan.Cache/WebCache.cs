﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ShiQuan.CacheBase
{
    /// <summary>
    /// Web 缓存接口实现
    /// </summary>
    public class WebCache : ICache
    {
        private static System.Web.Caching.Cache _Cache = HttpRuntime.Cache;
        /// <summary>
        /// 读取
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <returns></returns>
        public T Read<T>(string cacheKey) where T : class
        {
            if (_Cache[cacheKey] == null)
                return default(T);
            return (T)_Cache[cacheKey];
        }
        /// <summary>
        /// 移除
        /// </summary>
        /// <param name="cacheKey"></param>
        public void Remove(string cacheKey)
        {
            _Cache.Remove(cacheKey);
        }
        /// <summary>
        /// 清空
        /// </summary>
        public void RemoveAll()
        {
            IDictionaryEnumerator CacheEnum = _Cache.GetEnumerator();
            while (CacheEnum.MoveNext())
            {
                _Cache.Remove(CacheEnum.Key.ToString());
            }
        }
        /// <summary>
        /// 写入
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <param name="value"></param>
        public void Write<T>(string cacheKey, T value) where T : class
        {
            _Cache.Insert(cacheKey, value, null, DateTime.Now.AddMinutes(10), System.Web.Caching.Cache.NoSlidingExpiration);
        }
        /// <summary>
        /// 写入
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <param name="value"></param>
        /// <param name="expireTime"></param>
        public void Write<T>(string cacheKey, T value, DateTime expireTime) where T : class
        {
            _Cache.Insert(cacheKey, value, null, expireTime, System.Web.Caching.Cache.NoSlidingExpiration);
        }
        /// <summary>
        /// 写入
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <param name="value"></param>
        /// <param name="timeSpan"></param>
        public void Write<T>(string cacheKey, T value, TimeSpan timeSpan)where T:class
        {
            _Cache.Insert(cacheKey, value,null,DateTime.Now.AddMinutes(timeSpan.TotalMinutes), System.Web.Caching.Cache.NoSlidingExpiration);
        }
    }
}
