﻿using ShiQuan.CacheFactory;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShiQuan.CacheBase;
using System.Xml;

namespace ShiQuan.CacheFactory.Tests
{
    [TestClass()]
    public class UnitTest1
    {
        [TestMethod()]
        public void CacheTest()
        {
            ICache cache = CacheAccess.Cache();
            Assert.IsNotNull(cache);

            cache.Write<string>("cacheKey", "cacheKey");
            var value = cache.Read<string>("cacheKey");
            Assert.AreEqual(value, "cacheKey");
        }
    }
}
