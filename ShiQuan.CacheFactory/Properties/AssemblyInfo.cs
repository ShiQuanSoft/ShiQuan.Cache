﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// 有关程序集的一般信息由以下
// 控制。更改这些特性值可修改
// 与程序集关联的信息。
[assembly: AssemblyTitle("ShiQuan.CacheFactory")]
[assembly: AssemblyDescription("系统缓存工厂项目-根据配置调用缓存实现")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("实全软件科技有限公司")]
[assembly: AssemblyProduct("ShiQuan.CacheFactory")]
[assembly: AssemblyCopyright("Copyright © 实全软件科技有限公司 2019")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// 将 ComVisible 设置为 false 会使此程序集中的类型
//对 COM 组件不可见。如果需要从 COM 访问此程序集中的类型
//请将此类型的 ComVisible 特性设置为 true。
[assembly: ComVisible(false)]

// 如果此项目向 COM 公开，则下列 GUID 用于类型库的 ID
[assembly: Guid("d4363733-4d2c-4def-a235-9dbb232692e0")]

// 程序集的版本信息由下列四个值组成: 
//
//      主版本
//      次版本
//      生成号
//      修订号
//
// 可以指定所有值，也可以使用以下所示的 "*" 预置版本号和修订号
//通过使用 "*"，如下所示:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.2019.09021")]
[assembly: AssemblyFileVersion("1.0.2019.09021")]
