﻿using ShiQuan.CacheBase;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShiQuan.CacheFactory
{
    /// <summary>
    /// 缓存存储
    /// </summary>
    public class CacheAccess
    {
        /// <summary>
        /// 定义通用的Repository
        /// </summary>
        /// <returns></returns>
        public static ICache Cache()
        {
            //修改为支持Redis
            var cacheType = ConfigurationManager.AppSettings["CacheType"];
            //switch (cacheType)
            //{
            //    case "RedisCache":
            //        return new CacheRedis.RedisCache();
            //    case "WebCache":
            //        return new CacheBase.WebCache();
            //    default:
            //        return new CacheBase.WebCache();
            //}
            return ShiQuan.Unity.UnityHelper.Instance.GetResolve<ICache>(cacheType);
        }
    }
}
