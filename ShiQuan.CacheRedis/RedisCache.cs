﻿using ShiQuan.CacheBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShiQuan.CacheRedis
{
    /// <summary>
    /// Redis 缓存
    /// </summary>
    public class RedisCache : ICache
    {
        #region Key-Value
        /// <summary>
        /// 读取缓存
        /// </summary>
        /// <param name="cacheKey">键</param>
        /// <returns></returns>
        public T Read<T>(string cacheKey) where T : class
        {
            return RedisHelper.Get<T>(cacheKey);
        }
        /// <summary>
        /// 写入缓存
        /// </summary>
        /// <param name="value">对象数据</param>
        /// <param name="cacheKey">键</param>
        public void Write<T>(string cacheKey, T value) where T : class
        {
            RedisHelper.Set(cacheKey, value);
        }
        /// <summary>
        /// 写入缓存
        /// </summary>
        /// <param name="value">对象数据</param>
        /// <param name="cacheKey">键</param>
        /// <param name="expireTime">到期时间</param>
        public void Write<T>(string cacheKey, T value, DateTime expireTime) where T : class
        {
            RedisHelper.Set(cacheKey, value, expireTime);
        }
        /// <summary>
        /// 写入缓存
        /// </summary>
        /// <param name="value">对象数据</param>
        /// <param name="cacheKey">键</param>
        /// <param name="timeSpan">缓存时间</param>
        public void Write<T>(string cacheKey, T value, TimeSpan timeSpan) where T : class
        {
            RedisHelper.Set(cacheKey, value, timeSpan);
        }
        /// <summary>
        /// 移除指定数据缓存
        /// </summary>
        /// <param name="cacheKey">键</param>
        public void Remove(string cacheKey)
        {
            RedisHelper.Remove(cacheKey);
        }
        /// <summary>
        /// 移除全部缓存
        /// </summary>
        public void RemoveAll()
        {
            RedisHelper.RemoveAll();
        }
        #endregion
    }
}
