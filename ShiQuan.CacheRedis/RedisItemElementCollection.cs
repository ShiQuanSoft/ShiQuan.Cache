﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShiQuan.CacheRedis
{
    /// <summary>
    /// 配置项集合
    /// </summary>
    public class RedisItemElementCollection : ConfigurationElementCollection
    {

        protected override ConfigurationElement CreateNewElement()
        {
            return new RedisItemElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((RedisItemElement)element).WriteServer;
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMap;

            }
        }

        protected override string ElementName
        {
            get
            {

                return "RedisItem";

            }
        }
        public RedisItemElement this[int index]
        {

            get
            {
                return (RedisItemElement)BaseGet(index);
            }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

    }
}
