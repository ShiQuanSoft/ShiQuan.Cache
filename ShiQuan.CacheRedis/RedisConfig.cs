﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShiQuan.CacheRedis
{
    /// <summary>
    /// Redis 配置
    /// </summary>
    public sealed class RedisConfig : ConfigurationSection
    {
        /// <summary>
        /// 获取配置信息
        /// </summary>
        /// <returns></returns>
        public static RedisConfig GetConfig()
        {
            return GetConfig("redisConfig");
        }
        /// <summary>
        /// 获取配置信息
        /// </summary>
        /// <param name="sectionName">xml节点名称</param>
        /// <returns></returns>
        public static RedisConfig GetConfig(string sectionName)
        {
            RedisConfig section = (RedisConfig)ConfigurationManager.GetSection(sectionName);
            if (section == null)
                throw new ConfigurationErrorsException("Section " + sectionName + " is not found.");
            return section;
        }

        /// <summary>
        /// Gets or sets the mail servers.：映射新添加的节点mailServers节点；这个节点下还包含了若干个mailServer节点，因此它是一个集合类
        /// </summary>
        /// <value>The mail servers.</value>
        /// <remarks>Editor：v-liuhch CreateTime：2015/6/27 22:05:56</remarks>
        [ConfigurationProperty("redisItem", IsDefaultCollection = false)]
        public RedisItemElement RedisItem
        {

            get
            {
                return this["redisItem"] as RedisItemElement;
            }
            set
            {
                this["redisItem"] = value;
            }
        }
    }
}
